package javanam;

public class SavingsAccount extends Account {

    public static void main(String[] args) {
        System.out.println();
    }

    public SavingsAccount(String nameIn, double balanceIn) throws DodgyNameException {
        super(nameIn, balanceIn);
    }

    @Override
    public void addInterest() {
        setBalance(getBalance() * 1.4);
    }
    
}