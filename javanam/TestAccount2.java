package javanam;

public class TestAccount2 {

    public static void main(String[] args) {
        Account[] arrayOfAccounts = new Account[5];
        double[] amounts = { 23.0, 5444.5, 2.9, 345.1, 34.8 };
        String[] names = { "Picard", "Ryker", "Worf", "Troy", "Data" };
        for (int i = 0; i < arrayOfAccounts.length; i++) {
            try {
                arrayOfAccounts[i] = new SavingsAccount(names[i], amounts[i]);
            } catch (DodgyNameException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            System.out.println(arrayOfAccounts[i].getName() + "\t" + arrayOfAccounts[i].getBalance());
            arrayOfAccounts[i].addInterest();
            System.out.println("Modified balance: " + arrayOfAccounts[i].getBalance());
        }
    }
}