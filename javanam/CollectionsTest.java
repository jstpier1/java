package javanam;

import java.util.HashSet;

public class CollectionsTest {

    public static void main(String[] args) throws DodgyNameException {
        HashSet<Account> accounts;
        accounts = new HashSet<Account>();
        accounts.add(new SavingsAccount("Chris Dico", 1006.5));
        accounts.add(new SavingsAccount("Jacquie Pierre", 10506.5));
        accounts.add(new CurrentAccount("Tony De", 73467.5));
        for(Account a: accounts) {
            System.out.println(a.getName() + "\t" + a.getBalance());
            a.addInterest();
            System.out.println(a.getBalance());
        }
        
        
    }
    
}