package javanam;

public class TestInheritance {
    
    public static void main(String[] args) throws DodgyNameException {
        Account[] accounts = new Account[3];
        accounts[0] = new SavingsAccount("Account", 2);
        accounts[1] = new SavingsAccount("Savings Account", 4);
        accounts[2] = new CurrentAccount("Current Account", 6);
        for(int i = 0; i < accounts.length; i++) {
            accounts[i].addInterest();
            System.out.println(accounts[i].getName() + "\t" + accounts[i].getBalance());
        }
    }
}