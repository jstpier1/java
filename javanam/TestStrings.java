package javanam;

import java.util.Date;

public class TestStrings {
    
    public static void main(String[] args) {
        String str = "example.doc";
        System.out.println(str);
        str = "example.bak";
        System.out.println(str);

        String str2 = "albequerque";
        if(str.compareTo(str2) < 0)
            System.out.println(str);
        else if(str.compareTo(str2) > 0)
            System.out.println(str2);

        String ow = "ow";
        String phrase = "the quick brown fox swallowed down the lazy chicken";
        int index = phrase.indexOf(ow);
        int count = 0;
        while(index != -1) {
            count++;
            phrase = phrase.substring(index+1);
            index = phrase.indexOf(ow);
        }
        System.out.println("Ow appeared " + count + " times in the phrase");

        String original = "Live not on evil";
        String givenStr = original.toLowerCase().replaceAll("\\s+", "");
        System.out.println(givenStr);
        boolean palindrome = true;
        for(int i = 0; i < givenStr.length()/2; i++) {
            int end = givenStr.length()-(i+1);
            if(!givenStr.substring(i, i+1).equals(givenStr.substring(end, end+1))) {
                System.out.println(original + " is not a palindrome!");
                palindrome = false;
                break;
            }
        }
        if(palindrome)
            System.out.println(original + " is a palindrome");

        Date today = new Date();
        String day = today.toString();
        System.out.println(day);
        day = "08/25/2020";
        System.out.println(day);

        String article = new String("This is a test. I tried to use a real article and it didn't work. I am getting annoyed so I will give up if this doesn't. Work or I will erase your existence.");
        String sentences[] = article.split("\\.");
        System.out.println("The article has " + sentences.length + " sentences.");
        for(int i=0; i < sentences.length; i++) {
            //System.out.println(sentences[i]);
            String[] word1 = sentences[i].trim().split(" ");
            System.out.println("Sentence " + i + " has " + word1.length + " words.");
        }
        String[] words = article.split(" ");
        System.out.println("The article has " + words.length + " words.");
    }
}