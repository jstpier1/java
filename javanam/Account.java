package javanam;

public abstract class Account {
    private double balance;
    private String name;

    public static void main(String[] args) {
        /*Account[] arrayOfAccounts = new Account[5];
        double[] amounts = {23.0, 5444.5, 2.9, 345.1, 34.8};
        String[] names = {"Picard", "Ryker", "Worf", "Troy", "Data"};
        for(int i = 0; i < arrayOfAccounts.length; i++) {
            arrayOfAccounts[i] = new Account();
            arrayOfAccounts[i].setBalance(amounts[i]);
            arrayOfAccounts[i].setName(names[i]);
            System.out.println(arrayOfAccounts[i].getName() + "\t" + arrayOfAccounts[i].getBalance());
            arrayOfAccounts[i].addInterest();
            System.out.println("Modified balance: " + arrayOfAccounts[i].getBalance());
        }
        */
    }

    public Account() throws DodgyNameException {
        this("Jacquie", 50.0);
    }

    public Account(String nameIn, double balanceIn) throws DodgyNameException{
        balance = balanceIn;
        if(!nameIn.equals("Fingers"))
            name = nameIn;
        else
            throw new DodgyNameException();
    }

    public void setBalance(double newBalance) {
        balance = newBalance;
    }
    public double getBalance() {
        return balance;
    }
    public void setName(String newName) throws DodgyNameException{
        if(!newName.equals("Fingers"))
            name = newName;
        else
            throw new DodgyNameException();
    }
    public String getName() {
        return name;
    }

    public abstract void addInterest();
    
}