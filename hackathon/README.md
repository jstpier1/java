# Hackathon

This Hackathon is to create a Library in Java with Books, CDs, DVDs, and
Periodicals.

## Notes

I have Item as an abstract class and all the items in the library extend it.

## Improvements

- Create a Map to keep track of the items borrowed and when they are due
- Add fields into Item for due date
- Add method to borrow in the Item class
- Add an equals method for the Item, Book, CD, etc.