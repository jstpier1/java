package hackathon;

public class Periodical extends Item {
    // Cannot be borrowed
    private String publisher;
    private String date;

    public Periodical (String titleIn, String publisherIn, String dateIn) {
        super(titleIn);
        publisher = publisherIn;
        date = dateIn;
    }

    public void setPublisher(String publisherIn) {
        publisher = publisherIn;
    }
    public String getPublisher() {
        return publisher;
    }
    public void setDate(String dateIn) {
        date = dateIn;
    }
    public String getDate() {
        return date;
    }
    
    public String toString() {
        String retVal = super.toString() + " published by " + publisher + " on " + date + "\n";
        return retVal;
    }

}