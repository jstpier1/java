package hackathon;

public class CD extends Item {
    private String artist;
    private String recordLabel;
    // A lot of work to implement tracklist
    // private String[] tracklist;

    public CD (String artistIn, String titleIn, String recordLabelIn) {
        super(titleIn);
        artist = artistIn;
        recordLabel = recordLabelIn;
    }

    public void setArtist(String artistIn) {
     artist = artistIn;
    }
    public String getArtist() {
        return artist;
    }
    public void setRecordLabel(String recordLabelIn) {
        recordLabel = recordLabelIn;
    }
    public String getRecordLabelr() {
        return recordLabel;
    }
    
    public String toString() {
        String retVal = super.toString() + " by " + artist + " released by " + recordLabel + "\n";
        return retVal;
    }

}