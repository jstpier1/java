package hackathon;

public class DVD extends Item {
    private String director;
    private String rating; // Ex: rated R or PG-13

    public DVD (String directorIn, String titleIn, String ratingIn) {
        super(titleIn);
        director = directorIn;
        rating = ratingIn;
    }

    public void setDirector(String directorIn) {
        director = directorIn;
    }
    public String getDirector() {
        return director;
    }
    public void setRating(String ratingIn) {
        rating = ratingIn;
    }
    public String getRating() {
        return rating;
    }
    
    public String toString() {
        String retVal = super.toString() + " directed by " + director + " and rated " + rating + "\n";
        return retVal;
    }

}