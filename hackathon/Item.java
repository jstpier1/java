package hackathon;

public abstract class Item {
    private String title;
    private boolean available;

    public Item(String titleIn) {
        title = titleIn;
        available = true;
    }
    public void setTitle(String titleIn) {
        title = titleIn;
    }
    public String getTitle() {
        return title;
    }
    public void setAvailability(boolean availableIn) {
        available = availableIn;
    }
    public boolean getAvailability() {
        return available;
    }

    
    public String toString() {
        if(available) 
            return "Available:" + title;
        return "Unavailable:" + title;
    }
}