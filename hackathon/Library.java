package hackathon;

import java.util.ArrayList;

public class Library {
    // Users can add and remove items to Library
    // Can borrow items

    private ArrayList<Book> books;
    private ArrayList<CD> cds;
    private ArrayList<DVD> dvds;
    private ArrayList<Periodical> periodicals;

    public static void main(String[] args) {
        Library lib = new Library();
        // Add books to the Library
        Book b1 = new Book("Harper Lee", "To Kill a Mockingbird", "J.B. Lippincott & Co.");
        lib.addBook(b1);
        Book b2 = new Book("Mark Twain", "The Adventures of Tom Sawyer", "American Publishing Company");
        lib.addBook(b2);
        Book b3 = new Book("Mark Twain", "The Adventures of Huckleberry Finn", "Chatto & Windus");
        lib.addBook(b3);
        // Add CDs
        CD c1 = new CD("Taylor Swift", "Taylor Swift", "Big Machine");
        lib.addCD(c1);
        CD c2 = new CD("Taylor Swift", "Lover", "Republic");
        lib.addCD(c2);
        // Add DVDs
        DVD d1 = new DVD("Jon Favreau", "Iron Man", "PG-13");
        lib.addDVD(d1);
        DVD d2 = new DVD("Joe Johnston", "Captain America", "PG-13");
        lib.addDVD(d2);
        DVD d3 = new DVD("Patty Jenkins", "Wonder Woman", "PG-13");
        lib.addDVD(d3);
        // Add periodicals
        Periodical p1 = new Periodical("Vogue", "Conde Nast", "September 2020");
        lib.addPeriodicals(p1);
        Periodical p2 = new Periodical("Vogue", "Conde Nast", "August 2020");
        lib.addPeriodicals(p2);
        System.out.println(lib);
        lib.removeBook(b2);
        lib.removeDVD(d1);
        lib.borrow(c2);
        lib.borrow(p1);
        System.out.println(lib);
    }

    public Library() {
        books = new ArrayList<Book>();
        cds = new ArrayList<CD>();
        dvds = new ArrayList<DVD>();
        periodicals = new ArrayList<Periodical>();
    }

    public void addBook(Book b) {
        books.add(b);
    }
    public boolean removeBook(Book b) {
        return books.remove(b);
    }
    public void addCD(CD c) {
        cds.add(c);
    }
    public boolean removeCD(CD c) {
        return cds.remove(c);
    }
    public void addDVD(DVD d) {
        dvds.add(d);
    }
    public boolean removeDVD(DVD d) {
        return dvds.remove(d);
    }
    public void addPeriodicals(Periodical p) {
        periodicals.add(p);
    }
    public boolean removePeriodical(Periodical p) {
        return periodicals.remove(p);
    }

    public boolean borrow(Item it) {
        if(it instanceof Periodical) {
            System.out.println("You cannot borrow periodicals");
            return false;
        }
        else if(it.getAvailability()) {
            it.setAvailability(false);
            return true;
        }
        return false;
    }

    public String toString() {
        String retVal = "Books: " + books + "\n";
        retVal += "CDs: " + cds + "\n";
        retVal += "DVDs: " + dvds + "\n";
        retVal += "Periodicals: " + periodicals + "\n";
        return retVal;
    }



}