package hackathon;

public class Book extends Item {
    private String author;
    private String publisher;

    public Book (String authorIn, String titleIn, String publisherIn) {
        super(titleIn);
        author = authorIn;
        publisher = publisherIn;
    }

    public void setAuthor(String authorIn) {
        author = authorIn;
    }
    public String getAuthor() {
        return author;
    }
    public void setPublisher(String publisherIn) {
        publisher = publisherIn;
    }
    public String getPublisher() {
        return publisher;
    }

    public String toString() {
        String retVal = super.toString() + " by " + author + " published by " + publisher + "\n";
        return retVal;
    }
    
}